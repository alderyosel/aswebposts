# Cluster Kubernetes containerd

En este post vamos a generar un cluster kubernetes con 1 instancia master y 2 instancias worker utilizando el motor de ejecucion containerd (debido al retiro del soporte para docker)

![devops](img/devops.png)

## Requisitos
* 1 PC o Maquina virtual con 2 cores, 4GB de RAM y con Ubuntu Server 18.04+ instalado y actualizado
* 2 PC o Maquinas Virtuales con 2 cores, 2GB de RAM y con Ubuntu Server 18.04+ instalado y actualizado
* Conocimiento en instalacion y administracion basica de entornos linux

Debemos definir tambien un nombre de host para cada instancia ya sea durante la instalacion del sistema operativo o posterior a ella, en el caso del post los hostnames a usar seran ***kmaster, knode1, knode2***

Para el post tambien  utilizaremos 3 numeros de IP definidos por mi red local ***(192.168.0.100 kmaster, 192.168.0.111 knodo1, 192.168.0.112 knodo2)***, si siguen esta guia deben conocer los numeros de IP disponibles en su red local y asignarlos a sus instancias durante la instalacion del sistema operativo o posterior a ello

Tambien debe conocer la interface de red que usan sus instancias si no las conoce debera usar el comando ifconfig para verificarlas

## Instalacion

### Para las 3 PCs o Maquinas Virtuales

Primero debemos deshabilitar la particion swap en nuestras instancias, para ello editamos el archivo ***/etc/fstab*** como ROOT o SUDO y bloqueamos la linea correspondiente a nuestra particion swap colocando el simbolo # delante de la linea que contiene el elemento /swap.img, guardamos y salimos de la edicion

Luego de guardar el cambio ejecutamos los siguientes comandos para la deshabilitacion completa de la particion swap

    sudo swapoff -a
    sudo systemctl daemon-reload

Ahora debemos habilitar los modulos que requiere containerd creando el archivo ***containerd.conf*** con el siguiente comando:

    sudo cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
        overlay
        br_netfilter
        EOF

Ahora habilitamos los modulos incluidos en el archivo

    sudo modprobe overlay
    sudo modprobe br_netfilter

Tambien debemos crear el archivo ***99-kubernetes-cri.conf*** para habilitar el uso de las redes con kubernetes, esto con el siguiente comando:

    sudo cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
        net.bridge.bridge-nf-call-iptables  = 1
        net.ipv4.ip_forward                 = 1
        net.bridge.bridge-nf-call-ip6tables = 1
        EOF

ahora refrescamos la configuracion con el cambio realizado

    sudo sysctl --system

Para instalar el motor de ejecucion ***containerd*** primero instalaremos sus dependencias

    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg \
        lsb-release

luego de instalar las dependencias debemos habilitar el repositorio con los siguientes comandos:

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

    echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

Ahora actualizamos el repositorio, instalamos el motor de ejecucion containerd y lo reiniciamos

    sudo apt update

    sudo apt install containerd.io

    sudo systemctl restart containerd

Una ves reiniciado el servicio containerd generamos el archivo de configuracion por defecto con el siquiente comando:

    containerd config default | sudo tee /etc/containerd/config.toml

Para poder usar systemd con cgroupdriver debemos editar el archivo generado en el paso anterior ***/etc/containerd/config.toml*** buscar la etiqueta respectiva y realizar el siguiente cambio:

    [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
        ...
        [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
            SystemdCgroup = true

Una ves realizado el cambio debemos reiniciar el servicio nuevamente

    sudo systemctl restart containerd

Como paso siguiente instalaremos los binarios de kubernetes para lo cual debemos primero habilitar el repositorio respectivo

    sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg

    echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

Una ves habilitado el repositorio de kubernetes actualizamos e instalamos los binarios de kubernetes

    sudo apt-get update
    sudo apt-get install -y kubelet kubeadm kubectl
    sudo apt-mark hold kubelet kubeadm kubectl

Debe realizar estar tareas en todas las instancias para poder continuar al siguiente paso.


### Solo para la instancia master

Una ves realizada la instalacion de los binarios debemos iniciar la instancia master con el siguiente comando:

    sudo kubeadm init --control-plane-endpoint=192.168.0.100 --apiserver-advertise-address=192.168.0.100 --apiserver-cert-extra-sans="192.168.0.100" --node-name kmaster --pod-network-cidr=192.168.0.0/16

La salida de este comando es muy importante por lo que debera copiarla o guardarla por que sera necesaria para poder añadir los nodos al master, la salida es similar a la siguiente:

    Para añadir un nuevo control plane:

    kubeadm join 192.168.0.100:6443 --token a0w7h3.fklu7j033x9bymfu \
        --discovery-token-ca-cert-hash sha256:958c07c22773b939680980afc084beeca7d8ee21c6fbe099285fd08d29a20019 \
        --control-plane

    Para añadir un nuevo worker

    kubeadm join 192.168.0.100:6443 --token a0w7h3.fklu7j033x9bymfu \
        --discovery-token-ca-cert-hash sha256:958c07c22773b939680980afc084beeca7d8ee21c6fbe099285fd08d29a20019

El token que obtendran es diferente al token que se muestra en el ejemplo previo por lo que insisto deberan guardar la salida que obtienen, en caso de que no hubiesen podido guardarla deben volver a generarla con el siguiente comando:

    kubeadm token create --print-join-command

Ahora para poder usar los comandos de kubernetes como usuario no root debemos ejecutar los siguiente comandos en la instancia master:

    mkdir -p $HOME/.kube

    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

    sudo chown $(id -u):$(id -g) $HOME/.kube/config


La instalacion en el nodo master esta finalizada, ahora debemos de añadir un proveedor de redes para el correcto funcionamiento del cluster, en este caso utilizaremos el provedor de redes calico, para realizarlo debemos ejecutar el siguiente comando con su usuario no root

    kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml

El comando desplegara los componentes necesarios para la ejecucion del proveedor de redes, para poder asegurar el uso de ipv4 desde nuestra interface de red debemos configurarlo, para ello descargamos la plantilla de configuracion

    wget https://docs.projectcalico.org/manifests/custom-resources.yaml

ahora procederemos a editar la plantilla descargada agregando las lineas marcadas en el siguiente ejemplo

    apiVersion: operator.tigera.io/v1
    kind: Installation
    metadata:
      name: default
    spec:
      # Configures Calico networking.
      calicoNetwork:
        # Note: The ipPools section cannot be modified post-install.
        ipPools:
        - blockSize: 26
          cidr: 192.168.0.0/16
          encapsulation: VXLANCrossSubnet
          natOutgoing: Enabled
          nodeSelector: all()
        nodeAddressAutodetectionV4:   -----agregar esta linea
          interface: enp0s8           -----agregar esta linea con la interface de red a usar ej. eth0, enp0s3, etc

Una ves editada la plantilla realizamos el despliegue de la misma en kubernetes

    kubectl apply -f custom-resources.yaml

Ahora tenemos el proveedor de redes calico instalado el paso siguiente es añadir los nodos worker a cluster

### Solo para los nodos worker

Una ves que tenemos la instancia master ejecutandose debemos añadir los nodos worker a la misma utilizando el comando obtenido al iniciar la instancia master, el comando sera similar a este:

    sudo kubeadm join 192.168.0.100:6443 --token a0w7h3.fklu7j033x9bymfu \
        --discovery-token-ca-cert-hash sha256:958c07c22773b939680980afc084beeca7d8ee21c6fbe099285fd08d29a20019


Una ves realizadas estas tareas usted tendra un cluster kubernetes operando, para poder validar el correcto funcionamiento del mismo puede usar los comandos:

    kubectl get nodes
    kubectl cluster-info

***YDTG/01***
